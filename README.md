
**Demo Android Project of Stripe Payment Gateway**

Collecting credit card information
At some point in the flow of your app, you’ll want to obtain payment details from the user.
There are a couple ways to do this:

1 . Use Google Pay to access your customer’s stored card information

2 . Use our built-in card input widget to collect card information

3 . Build your own credit card form