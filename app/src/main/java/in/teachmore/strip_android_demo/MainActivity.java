package in.teachmore.strip_android_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;


public class MainActivity extends AppCompatActivity {

    Context mContext;
    CardInputWidget mCardInputWidget;
    Button btnPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        bindData();
        setClickListener();
    }

    private void bindData() {
         mCardInputWidget = findViewById(R.id.card_input_widget);
         btnPay = findViewById(R.id.btn_pay);
    }


    private void setClickListener() {

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getData();

            }
        });
    }


    /**
     * As you can see in the example above,
     * the Card instance contains some helpers to validate that the card number passes the Luhn check,
     * that the expiration date is the future,
     * and that the CVC looks valid. You’ll probably want to validate these three things at once,
     * so we’ve included a validateCard function that does so.
     */

    private void getData() {

        // Here we have two options to get card's data
        // 1. If we are using CardInputWidget then we can simply get data by mCardInputWidget.getCard();
        // 2. If we creates an own form then we can collect as below
        Card card = new Card("4242-4242-4242-4242", 12, 2020, "123");


        if(!card.validateCard()){

            // show errors
            if(!card.validateNumber()){

                // show errors

            }

            if(!card.validateExpiryDate()){

                // show errors

            }

            if(!card.validateExpMonth()){

                // show errors

            }

            if(!card.validateCVC()){

                // show errors

            }
        }else {

            Stripe stripe = new Stripe(mContext, "pk_test_TYooMQauvdEDq54NiTphI7jx");
            stripe.createToken(
                    card,
                    new TokenCallback() {
                        public void onSuccess(Token token) {
                            // Send token to your server
                            sendTokenToServer();
                        }
                        public void onError(Exception error) {
                            // Show localized error message
                            Toast.makeText(mContext,
                                    error.getLocalizedMessage(),
                                    Toast.LENGTH_LONG
                            ).show();
                        }
                    }
            );
        }
    }

    private void sendTokenToServer() {

        // Send token to your server

    }


}
